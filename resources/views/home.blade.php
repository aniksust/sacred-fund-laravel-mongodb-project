@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>
                <br>
                <div style="padding-top: auto">
                <table class="table align-self-auto">
                    <thead>
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">Title</th>
                        <th scope="col">Content</th>
                        <th scope="col">Updated at</th>
                        <th scope="col">Created by</th>
                        <th scope="col">Action</th>
                    </tr>
                    </thead>
                    <tbody >
                    <?php $no=1;?>
                    @foreach($post as $posts)
                    <tr>
                        <th scope="row">{{$no}}</th>
                        <td>{{$posts->title}}</td>
                        <td>{{$posts->content   }}</td>
                        <td>{{$posts->updated_at}}</td>
                        <td>{{$posts->created_by}}</td>

                        <td>
                            <a href="{{route('post.form',$posts->_id)}}" class="btn btn-success btn-sm"> Update</a>
                            <a href="{{route('post.delete',$posts->_id)}}" class="btn btn-danger btn-sm"> Delete</a>
                        </td>
                    </tr>
                        <?php $no++; ?>
                        @endforeach
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
