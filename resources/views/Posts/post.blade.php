@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Post form</div>

                @if($data ?? '')

                <form action="{{route('post.update', $data->_id)}}" method="post">
                    @csrf
                    @method('put')
                    <div class="form-group">
                        <label for="Title">Title</label>
                        <input type="text" class="form-control" name="title" aria-describedby="emailHelp" placeholder="Enter Title" value="{{$data->title}}">
                        {{--<small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>--}}
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Body</label>
                        <textarea type="text" class="form-control" name="content" >{{$data->content}}</textarea>
                    </div>
                    {{--<div class="form-group form-check">--}}
                        {{--<input type="checkbox" class="form-check-input" id="exampleCheck1">--}}
                        {{--<label class="form-check-label" for="exampleCheck1">Check me out</label>--}}
                    {{--</div>--}}
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>

                    @else
                    <form action="{{route('post.save')}}" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="Title">Title</label>
                            <input type="text" class="form-control" name="title" aria-describedby="emailHelp" placeholder="Enter Title">
                            {{--<small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>--}}
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Body</label>
                            <textarea type="text" class="form-control" name="content" placeholder="Body"></textarea>
                        </div>
                        {{--<div class="form-group form-check">--}}
                        {{--<input type="checkbox" class="form-check-input" id="exampleCheck1">--}}
                        {{--<label class="form-check-label" for="exampleCheck1">Check me out</label>--}}
                        {{--</div>--}}
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                    @endif
            </div>
        </div>
    </div>




</div>
@endsection
