<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
//            $table->increments('id');
            $table->string('user_id');
            $table->string('image')->nullable();;
            $table->string('title')->nullable();;
            $table->string('blurb')->nullable();;
            $table->string('category')->nullable();;
            $table->string('subcategory')->nullable();;
            $table->string('location')->nullable();;
            $table->string('duration')->nullable();;
            $table->string('goal')->nullable();;
            $table->string('video')->nullable();;
            $table->string('risks')->nullable();;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
