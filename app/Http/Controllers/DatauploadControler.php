<?php
namespace App\Http\Controllers;
use App\description;
use App\Project;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use \Input as Input;
class DatauploadControler extends Controller
{

    public function insertform()
    {
        return view('/about');
    }


    public function basic(Request $request)
    {
        $data = new Project($request->all());
        $data->created_by = Auth::user()->id;


        if ($request->hasFile('fileupload')) {
            $photo = $request->file('fileupload');
            $photoname = time() . '_' . rand() . '.' . $photo->getClientOriginalExtension();
            $location = public_path('images/fileupload' . $photoname);
            //Image::make($photo)->resize(600, 600)->save($location);
            $photo->move($location, $photoname);
            $data->fileupload = $photoname;

        }

        if ($data->softtype == 1)
            $data->softtype = 'Android Apps';
        elseif ($data->softtype == 2)
            $data->softtype = 'Website';
        elseif ($data->softtype == 3)
            $data->softtype = 'Research';
        elseif ($data->softtype == 4)
            $data->softtype = 'Robotics';
        elseif ($data->softtype == 5)
            $data->softtype = 'Web Apps';
        elseif ($data->softtype == 6)
            $data->softtype = 'Games';
        elseif ($data->softtype == 7)
            $data->softtype = 'Design & Tech';
        elseif ($data->softtype == 8)
            $data->softtype = 'Services';
        elseif ($data->softtype == 9)
            $data->softtype = 'Business Idea';
        elseif ($data->softtype == 10)
            $data->softtype = 'Electorics';
        elseif ($data->softtype == 11)
            $data->softtype = 'Networking';
        elseif ($data->softtype == 12)
            $data->softtype = 'Desktop Softwares';


        $data->save();
        if ($data) {
//        return redirect()->route('home');
            return redirect()->route('edit02');

        } else {
            return back();
        }


    }


    public function story(Request $request)
    {
        $data = new description($request->all());
        $data->created_by = Auth::user()->id;


        if ($request->hasFile('fileupload')) {
            $photo = $request->file('fileupload');
            $photoname = time() . '_' . rand() . '.' . $photo->getClientOriginalExtension();
            $location = public_path('images/fileupload' . $photoname);
            //Image::make($photo)->resize(600, 600)->save($location);
            $photo->move($location, $photoname);
            $data->fileupload = $photoname;

        }


        $data->save();
        if ($data) {


//        return redirect()->route('home');
            return redirect()->route('homedisplay')->with('message', 'Thanks for submitting your project!!');

        } else {
            return back();
        }


    }


    public function about(Request $request)
    {
        $user_id = Auth::user()->id;


        $picture = $request->file('fileupload2');
        $biography = $request->input('biography');
        $website = $request->input('websites');
        $location = $request->input('location');
        $contact = $request->input('contact');
        $accounttype = $request->input('accountname');
        $accountnumber = $request->input('accountnumber');

        $input = time();

        $destinationPath = public_path('/upload');


        $picture->move($destinationPath, $user_id . $input . '.' . $picture->getClientOriginalExtension());


        $val = 'upload/' . $user_id . $input . '.' . $picture->getClientOriginalExtension();


        DB::table('users')->where('id', $user_id)->update(['picture' => $val, 'biography' => $biography, 'website' => $website, 'location' => $location, 'contact' => $contact, 'accounttype' => $accounttype, 'accountnumber' => $accountnumber]);

        return redirect('/');

    }


    public function update()
    {

    }

    public function comment()
    {

    }


    public function insert(Request $request)
    {


    }
}
