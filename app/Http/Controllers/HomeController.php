<?php

namespace App\Http\Controllers;

use App\post;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $post = post::all();
        return view('home', compact('post'));
    }

    public function edit_story()
    {
//        $post = post::all();
        return view('edit-story');
    }

    public function about_me()
{
//        $post = post::all();
    return view('edit-about');
}
}
