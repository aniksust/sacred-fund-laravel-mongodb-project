<?php

namespace App\Http\Controllers;

use App\post;
use http\Message;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
//use Jenssegers\Mongodb\Auth as Auth;

class PostController extends Controller
{
   public function form($_id = false){

       if ($_id){
           $data = post::findOrFail($_id);
           return view('Posts.post', compact('data'));
       }
       return view('Posts.post');
   }

   public function save(Request $request){

//       dd($request);

       $data = new Post($request->all());
       $data->created_by =Auth::user()->email;
       $data->save();
       if ($data){
           return redirect()->route('home');
       }
       else{
           return back();
       }
   }

    /**
     * @param Request $request
     * @param $_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $_id){

       $data = post::findOrFail($_id);

       $data->title = $request->title;
       $data->content = $request->content;

       $data->save();
       if ($data){
           return redirect()->route('home');
       }
       else{
           return back();
       }
   }


   public function delete($_id){
        $data = post::destroy($_id);
       if ($data){
           return redirect()->route('home');
       }
       else{
           return back();
       }


   }


}
