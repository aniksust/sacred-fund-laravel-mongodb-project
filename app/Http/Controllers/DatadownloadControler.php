<?php
namespace App\Http\Controllers;
use App\description;
use App\Project;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Jenssegers\Mongodb\Auth\User as Authenticatable;


class DatadownloadControler extends Controller {

   public function insertform(){
      return view('/about');
   }


   public function homedisplay()
{

    $projects = Project::all();
//    dd($projects);
    return view('index', compact('projects'));





}


public function passproject(Request $request,$id = null){

$request->session()->put('dataid', $id);

return redirect('project');
   }



public function showproject(Request $request){

$var=$request->session()->get('dataid');
$data = DB::table('projects')->select('id','user_id','image','title','blurb','category','duration','goal','video')->where(['id'=>$var])->get();

$data2 = DB::table('users')->select('id','first_name','location','picture')->where(['id'=>$data[0]->user_id])->get();

$data3 = DB::table('description')->select('description','image')->where(['project_id'=>$var])->get();


    return view('project')->with( ['project'=>$data,'user'=>$data2,'story'=>$data3]);;

}

public function editstory(Request $request){

$projectvideo = $request->input('projectvideo');
$projectdescription=$request->input('projectdescription');
$risksandchallenges=$request->input('risksandchallenges');



$id=$request->session()->get('dataid');

DB::table('basic')->where('id', $id)->update(['projectdescription'=>$projectdescription,'risksandchallenges'=>$risksandchallenges]);

  return redirect('/edit-perks');
}

public function editperks(Request $request){


  $title = $request->input('title');
$pledgeamount=$request->input('pledgeamount');
$description=$request->input('description');
$estimateddelivery=$request->input('estimateddelivery');
$shippingdetails=$request->input('shippingdetails');



$id=$request->session()->get('dataid');

 DB::table('perks')->insertGetId(
        array('title'=>$title,'pledgeamount'=>$pledgeamount,'description'=>$description,'estimated delivery'=>$estimateddelivery,'shippingdetails'=>$shippingdetails,'basicid'=>$id)
    );


  return redirect('/index');
}

public function editdiscard(Request $request){

  return redirect('/index');
}


	
   public function insert(Request $request){
$email = $request->input('Email');
$firstname=$request->input('FirstName');
$lastname=$request->input('LastName');
$password = $request->input('Password');
$confirmpassword=$request->input('ConfirmPassword');
	
     // DB::insert('insert into student (name) values(?)',[$name]);
echo $confirmpassword;
if(strcmp($password,$confirmpassword)==0)
{
	DB::insert('insert into profile (FirstName,LastName,Email,Password) values(?,?,?,?)',[$firstname,$lastname,$email,$password]);

	DB::insert('insert into users (name,Email,Password) values(?,?,?)',[$firstname,$email,$password]);

	return redirect('/about');
}
   }

    public function single($_id = false){

        if ($_id){
            $data = Project::findOrFail($_id);
//            $data2 = description::findOrFail($_id);
            return view('Project', compact('data'));
        }
//        dd($data);
        return view('project');
    }

}
