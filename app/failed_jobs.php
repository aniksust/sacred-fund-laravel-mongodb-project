<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
class failed_jobs extends Eloquent
{
    protected $connection = 'mongodb';

    protected $guarded = [];

}
