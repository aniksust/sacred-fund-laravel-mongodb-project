<?php

namespace App;
use Illuminate\Contracts\Auth\MustVerifyEmail;
//use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;

class Project extends Eloquent
{
//    use AuthenticableTrait;
    protected $connection = 'mongodb';

    protected $guarded = [];


    public function User(){
        return $this->belongsTo('App\User', 'created_by');
    }
}
